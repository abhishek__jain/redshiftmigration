package com.freecharge.migration.redshift;

import java.sql.*;
import java.util.Properties;

public class MangoConnection implements RedshiftConnection {

    public String getUsername() {
        return "fruitfunch";
    }

    public String getPassword() {
        return "Iamrootuserformangocluster1";
    }

    public String getDbUrl() {
        return "jdbc:postgresql://rsc.freecharge.com:5439/freecharge?tcpKeepAlive=true";
    }

    public Connection getConnection() throws SQLException {
        Properties props = new Properties();

        props.setProperty("user", getUsername());
        props.setProperty("password", getPassword());
        return DriverManager.getConnection(getDbUrl(), props);
    }

    public ResultSet executeSql(String sql) throws SQLException {

        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (Exception ex) {

            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception ex) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return rs;
        }
    }
}
