package com.freecharge.migration.redshift;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface RedshiftConnection {

    String getUsername();

    String getPassword();

    String getDbUrl();

    Connection getConnection() throws SQLException;

    ResultSet executeSql(String sql) throws SQLException;
}
